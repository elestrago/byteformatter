﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime.MigrateHelper;
using Package.ByteFormatter.Generator.Utils;
using Package.ByteFormatter.VersionDiff.Contexts;

namespace Package.ByteFormatter.VersionDiff
{
	public static class ByteDataDiffService
	{
		public static List<VersionData> GetVersions()
			=> AssemblyHelper.GetTypes()
				.Where(f => f.GetCustomAttribute<DataVersionAttribute>() != null)
				.Select(type => (type.GetCustomAttribute<DataVersionAttribute>(), type))
				.GroupBy(v => v.Item1.Version)
				.Select(group => new VersionData
				{
					Version = group.Key,
					Contexts = group
						.Select(s => new StateDataContext(s.Item1.TypeName, s.type))
						.Cast<ADataContext>()
						.ToList()
				}).ToList();

		public static List<Diff> Diff(string fromVersion, string toVersion)
		{
			var versionDatas = GetVersions();
			var fromVersionData = versionDatas.Find(f => f.Version == fromVersion);
			var toVersionData = versionDatas.Find(f => f.Version == toVersion);
			var diffs = new List<Diff>();

			foreach (var fromContext in fromVersionData.Contexts)
			{
				var toContext = toVersionData.Contexts.Find(f => f.Equals(fromContext));
				var diff = toContext != null
					? Diff(fromContext, toContext)
					: Diff(fromContext, ADataContext.Empty);
				diffs.Add(diff);
			}

			foreach (var toContext in toVersionData.Contexts.Except(fromVersionData.Contexts))
			{
				diffs.Add(Diff(ADataContext.Empty, toContext));
			}

			return diffs;
		}

		private static Diff Diff(ADataContext srcContext, ADataContext dstContext)
		{
			var diffs = new List<Diff>();
			var srcChildContexts = srcContext.ChildContexts;
			var dstChildContexts = dstContext.ChildContexts;

			var srcExcluded = new List<int>();
			var dstStartIndex = 0;
			for (var i = 0; i < srcChildContexts.Count; i++)
			{
				var srcChildContext = srcChildContexts[i];
				for (var j = dstStartIndex; j < dstChildContexts.Count; j++)
				{
					dstStartIndex++;
					var dstChildContext = dstChildContexts[j];
					if (srcChildContext.Name != dstChildContext.Name)
					{
						if (srcChildContexts.All(c => c.Name != dstChildContext.Name))
						{
							var addedDiff = Diff(ADataContext.Empty, dstChildContext);
							diffs.Add(new Diff($"{j}: {dstChildContext.Name}", EDiffType.Added, addedDiff.ChildDiffs));
							continue;
						}

						if (dstChildContexts.All(c => c.Name != srcChildContext.Name))
						{
							dstStartIndex--;
							var removedDiff = Diff(dstChildContext, ADataContext.Empty);
							diffs.Add(new Diff($"{i}: {srcChildContext.Name}", EDiffType.Removed,
								removedDiff.ChildDiffs));
							break;
						}

						var moved = default(ADataContext);
						for (int k = 0; k < srcChildContexts.Count; k++)
						{
							if (srcExcluded.Contains(k))
								continue;

							var nextChildContext = srcChildContexts[k];
							if (nextChildContext.Name != dstChildContext.Name)
								continue;

							srcExcluded.Add(k);
							moved = nextChildContext;
							break;
						}

						if (moved != null)
						{
							var movedDiff = Diff(moved, dstChildContext);
							diffs.Add(new Diff($"{j} <- {srcExcluded.Last()}: {dstChildContext.Name}", EDiffType.Moved,
								movedDiff.ChildDiffs));
							break;
						}
					}

					var childDiff = Diff(srcChildContext, dstChildContext);
					var diffType = childDiff.ChildDiffs.Count > 0 ? EDiffType.Changed : EDiffType.None;
					diffs.Add(new Diff($"{j}: {srcChildContext.Name}", diffType, childDiff.ChildDiffs));
					break;
				}
			}

			for (var j = dstStartIndex; j < dstChildContexts.Count; j++)
			{
				var dstChildContext = dstChildContexts[j];
				var addedDiff = Diff(ADataContext.Empty, dstChildContext);
				diffs.Add(new Diff($"{j}: {dstChildContext.Name}", EDiffType.Added, addedDiff.ChildDiffs));
			}

			if (diffs.All(d => d.DiffType == EDiffType.None))
				return new Diff(dstContext.Name, EDiffType.None);

			if (srcContext.Equals(ADataContext.Empty))
				return new Diff(dstContext.Name, EDiffType.Added, diffs);

			if (dstContext.Equals(ADataContext.Empty))
				return new Diff(srcContext.Name, EDiffType.Removed, diffs);

			return new Diff(dstContext.Name, EDiffType.Changed, diffs);
		}
	}
}