﻿using System;
using System.Collections.Generic;
using System.Linq;
using ByteFormatter.Runtime;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;
using NUnit.Framework;
using Package.ByteFormatter.ByteValidator.Impls;

namespace Package.ByteFormatter.ByteValidator
{
    public class ByteConvetableValidator
    {
        public static void Validate()
        {
            var byteConvertableTypes = AppDomain.CurrentDomain.GetAssemblies().Select(s => s.GetTypes())
                .Aggregate((types, types1) => types.Concat(types1).ToArray())
                .Where(t => t.ImplementsInterface<IByteConvertable>())
                .ToArray();

            var errors = new List<string>();
            foreach (var convertableType in byteConvertableTypes)
            {
                errors.AddRange(ValidateType(convertableType));
            }

            foreach (var error in errors)
            {
                UnityEngine.Debug.Log($"[{nameof(ByteConvetableValidator)}] <color=red>{error}</color>");
            }

            if (errors.Count == 0)
                UnityEngine.Debug.Log($"[{nameof(ByteConvetableValidator)}] Success");
            else
                Assert.That(false);
        }

        private static List<string> ValidateType(Type convertableType)
        {
            var errors = new List<string>();
            var variants = DataProviderService.GetDataVariants(convertableType);

            for (var i = 0; i < variants; i++)
            {
                var expectedSize = DataProviderService.GetDataSize(i, convertableType);
                var writer = new ByteWriter();
                var convertable = (IByteConvertable) Activator.CreateInstance(convertableType);
                DataProviderService.SetData(i, convertable);
                var enumerator = convertable.ToByte(writer);
                enumerator.Enumerate();

                var bytes = writer.ToArray();

                if (expectedSize != bytes.Length)
                {
                    errors.Add(
                        $"Data size not valid fot type {convertableType.Name}. Expected size {expectedSize} current {bytes.Length}");
                }

                try
                {
                    var reader = new ByteReader(bytes);
                    convertable = (IByteConvertable) Activator.CreateInstance(convertableType);
                    convertable.FromByte(reader);
                    errors.AddRange(DataProviderService.AssertDataEqual(i, convertable));
                }
                catch (Exception e)
                {
                    errors.Add($"Cannot read from bytes: {convertableType.Name} : {e.StackTrace}");
                }
            }

            if (errors.Count > 0)
                errors.Insert(0, $"------ Test {convertableType.Name} FAIL -------");

            return errors;
        }
    }
}