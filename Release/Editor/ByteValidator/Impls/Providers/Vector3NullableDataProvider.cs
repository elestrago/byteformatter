﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class Vector3NullableDataProvider : ADataProvider<Vector3?>
	{
		protected override Vector3?[] DataArray { get; } = {null, Vector3.one};
		protected override int[] DaraSizeArray { get; } = {1, 13};
	}
}