﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class UShortDataProvider : ADataProvider<ushort>
	{
		protected override ushort[] DataArray { get; } = { ushort.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 2 };
	}
}