﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public class BoundsNullableDataProvider : ADataProvider<Bounds?>
	{
		protected override Bounds?[] DataArray { get; } = {null, new Bounds(Vector3.one, Vector3.one)};
		protected override int[] DaraSizeArray { get; } = {1, 25};
	}
}