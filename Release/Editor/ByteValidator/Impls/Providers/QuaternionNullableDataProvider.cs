using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class QuaternionNullableDataProvider : ADataProvider<Quaternion?>
	{
		protected override Quaternion?[] DataArray { get; } =
		{
			null,
			Quaternion.identity
		};

		protected override int[] DaraSizeArray { get; } = {1, 17};
	}
}