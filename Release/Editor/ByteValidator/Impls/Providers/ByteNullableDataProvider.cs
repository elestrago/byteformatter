namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class ByteNullableDataProvider : ADataProvider<byte?>
	{
		protected override byte?[] DataArray { get; } = { null, 255 };
		protected override int[] DaraSizeArray { get; } = { 1, 2 };
	}
}