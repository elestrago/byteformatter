﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class ULongDataProvider : ADataProvider<ulong>
	{
		protected override ulong[] DataArray { get; } = { ulong.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 8 };
	}
}