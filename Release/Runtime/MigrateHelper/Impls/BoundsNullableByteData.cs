﻿using UnityEngine;

namespace ByteFormatter.Runtime.MigrateHelper.Impls
{
	[ByteDataTypeAccess(typeof(Bounds?))]
	public class BoundsNullableByteData : AByteData<BoundsNullableByteData>
	{
		public override void Transfer(ByteReader reader, ByteWriter writer)
			=> writer.Write(reader.ReadBoundsNullable());

		public override void Skip(ByteReader reader)
			=> reader.SkipBoundsNullable();
	}
}