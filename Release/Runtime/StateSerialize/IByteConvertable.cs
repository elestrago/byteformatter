﻿using System.Collections;

namespace ByteFormatter.Runtime.StateSerialize
{
	public interface IByteConvertable
	{
		IEnumerator ToByte(ByteWriter writer);

		void FromByte(ByteReader reader);
	}
}