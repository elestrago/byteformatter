using System;

namespace ByteFormatter.Runtime
{
	public class FloatConversion
	{
		public static float ToSingle(uint value)
			=> new UIntFloat() {intValue = value}.floatValue;

		public static double ToDouble(ulong value)
			=> new UIntFloat() {longValue = value}.doubleValue;

		public static Decimal ToDecimal(ulong value1, ulong value2)
			=> new UIntDecimal()
			{
				longValue1 = value1,
				longValue2 = value2
			}.decimalValue;
	}
}