using System;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime;

namespace States
{
    //------------------------------------------------------------------------------
    // <auto-generated>
    //     This code was generated by StateSerializeGenerator.
    //
    //     Changes to this file may cause incorrect behavior and will be lost if
    //     the code is regenerated.
    // </auto-generated>
    //------------------------------------------------------------------------------
    public partial class UserAddressState : IByteConvertable
    {
        public System.Collections.IEnumerator ToByte(ByteWriter writer)
        {
			writer.Write(Value);
			yield return null;
        }

        public void FromByte(ByteReader reader)
        {
			Value = reader.ReadInt32();
        }
    }
}