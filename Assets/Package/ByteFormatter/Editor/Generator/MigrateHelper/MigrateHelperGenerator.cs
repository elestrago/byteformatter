﻿using System.IO;
using UnityEngine;

namespace Package.ByteFormatter.Generator.MigrateHelper
{
	public static class MigrateHelperGenerator
	{
		public static void Generate(DirectoryInfo directoryInfo)
		{
			directoryInfo = new DirectoryInfo(Path.Combine(directoryInfo.FullName, "v" + Application.version));
			var generator = new MigrateHelperByteDataGenerator();
			var generationProvider = new ByteDataGenerationProvider(generator);
			generationProvider.Generate(directoryInfo);
		}
	}
}