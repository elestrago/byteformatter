﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Package.ByteFormatter.VersionDiff
{
	public class MigrateHelperVersionDiffEditorWindow : EditorWindow
	{
		private List<VersionData> _versionDatas;

		private string[] _versions;
		private int _fromVersion;
		private int _toVersion;
		private List<Diff> _diffs;

		private Vector2 _scrollPosition;

		[MenuItem("Tools/Migrator/Version Diff")]
		public static void OpenSettingsWindowButton()
		{
			var window = GetWindow<MigrateHelperVersionDiffEditorWindow>(false, "Migrator");
			window.Show();
		}

		private void OnEnable()
		{
			_versionDatas = ByteDataDiffService.GetVersions();
			_versions = _versionDatas.Select(s => s.Version).ToArray();
		}

		private void OnGUI()
		{
			GUILayout.Space(5);
			GUILayout.BeginHorizontal();
			GUILayout.Label("From");
			_fromVersion = EditorGUILayout.Popup(_fromVersion, _versions);
			GUILayout.Label("To");
			_toVersion = EditorGUILayout.Popup(_toVersion, _versions);
			GUILayout.EndHorizontal();

			GUILayout.Space(5);
			if (GUILayout.Button("Diff"))
				Diff();

			DrawDiff();
		}

		private void DrawDiff()
		{
			if (_diffs == null)
			{
				GUILayout.Label("Press on button \"Diff\"");
				return;
			}

			if (_diffs.All(f => f.DiffType == EDiffType.None))
			{
				GUILayout.Label("No diffs");
				return;
			}

			_scrollPosition = GUILayout.BeginScrollView(_scrollPosition, false, false);
			foreach (var diff in _diffs)
			{
				if (diff.DiffType == EDiffType.None)
					continue;

				DrawDiff(diff, true);
			}

			GUILayout.EndScrollView();
		}

		private static void DrawDiff(Diff diff, bool isRoot = false)
		{
			var color = GetDiffColor(diff.DiffType);
			GUI.contentColor = color;
			EditorGUILayout.BeginVertical(GUI.skin.box);
			GUILayout.Label($"{diff.Name} ({diff.DiffType})", isRoot ? EditorStyles.boldLabel : EditorStyles.label);
			if (diff.ChildDiffs.Count > 0)
			{
				GUILayout.BeginHorizontal();
				GUILayout.Space(25);

				GUILayout.BeginVertical();
				foreach (var diffChild in diff.ChildDiffs)
					DrawDiff(diffChild);
				GUILayout.EndVertical();

				GUILayout.Space(5);
				GUILayout.EndHorizontal();
			}

			EditorGUILayout.EndVertical();
		}

		private void Diff()
		{
			var fromVersion = _versions[_fromVersion];
			var toVersion = _versions[_toVersion];
			_diffs = ByteDataDiffService.Diff(fromVersion, toVersion);
		}

		private static Color GetDiffColor(EDiffType diffType)
		{
			Color rgb;
			switch (diffType)
			{
				case EDiffType.None:
					rgb = Color.gray;
					break;
				case EDiffType.Added:
					rgb = Color.green;
					break;
				case EDiffType.Removed:
					rgb = Color.red;
					break;
				case EDiffType.Changed:
					rgb = Color.yellow;
					break;
				case EDiffType.Moved:
					rgb = Color.cyan;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(diffType), diffType, null);
			}

			return rgb;
		}
	}
}