﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class RectIntNullableDataProvider : ADataProvider<RectInt?>
	{
		protected override RectInt?[] DataArray { get; } = {null, new RectInt(1, 1, 1, 1)};
		protected override int[] DaraSizeArray { get; } = {1, 17};
	}
}