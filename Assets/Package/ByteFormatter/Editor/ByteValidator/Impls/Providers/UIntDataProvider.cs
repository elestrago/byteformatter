﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class UIntDataProvider : ADataProvider<uint>
	{
		protected override uint[] DataArray { get; } = { uint.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 4 };
	}
}