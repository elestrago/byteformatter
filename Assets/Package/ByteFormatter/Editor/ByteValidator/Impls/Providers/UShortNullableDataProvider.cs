namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class UShortNullableDataProvider : ADataProvider<ushort?>
	{
		protected override ushort?[] DataArray { get; } = { null, ushort.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 1, 3 };
	}
}