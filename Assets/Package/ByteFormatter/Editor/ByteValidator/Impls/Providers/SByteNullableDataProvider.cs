namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class SByteNullableDataProvider : ADataProvider<sbyte?>
	{
		protected override sbyte?[] DataArray { get; } = { null, 127 };
		protected override int[] DaraSizeArray { get; } = { 1, 2 };
	}
}