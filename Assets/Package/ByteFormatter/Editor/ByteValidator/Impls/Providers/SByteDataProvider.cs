﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class SByteDataProvider : ADataProvider<sbyte>
	{
		protected override sbyte[] DataArray { get; } = { 127 };
		protected override int[] DaraSizeArray { get; } = { 1 };
	}
}