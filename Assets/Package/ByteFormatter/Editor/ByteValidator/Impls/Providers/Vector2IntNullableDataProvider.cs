﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	[ByteValidatorDataProvider]
	public sealed class Vector2IntNullableDataProvider : ADataProvider<Vector2Int?>
	{
		protected override Vector2Int?[] DataArray { get; } = {null, Vector2Int.one};
		protected override int[] DaraSizeArray { get; } = {1, 9};
	}
}