﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;
using Package.ByteFormatter.Generator;
using Package.ByteFormatter.Generator.Utils;
using UnityEditor;
using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls
{
	[InitializeOnLoad]
	public static class DataProviderService
	{
		private static readonly List<IDataProvider> DataProviders;

		static DataProviderService()
		{
			DataProviders = AssemblyHelper.GetTypes()
				.Where(type => type.HasAttribute<ByteValidatorDataProviderAttribute>()
				               && type.ImplementsInterface<IDataProvider>())
				.Select(Activator.CreateInstance)
				.Cast<IDataProvider>()
				.ToList();
		}

		public static int GetDataVariants(Type type)
		{
			if (type.GetInterface(typeof(IByteConvertable).FullName) != null)
				return type.GetSerializedProperties()
					.Select(propertyInfo => GetProvider(propertyInfo).GetDataVariants(propertyInfo.PropertyType))
					.Max();

			var provider = DataProviders.FirstOrDefault(f => f.CanProvide(type));
			if (provider == null)
				throw new Exception($"[{nameof(DataProviderService)}] Cannot find data provider for type {type}");

			return provider.GetDataVariants(type);
		}

		public static int GetDataSize(int variantNumber, Type type)
		{
			if (type.GetInterface(typeof(IByteConvertable).FullName) == null)
			{
				var provider = DataProviders.FirstOrDefault(f => f.CanProvide(type));
				if (provider == null)
					throw new Exception($"[{nameof(DataProviderService)}] Cannot find data provider for type {type}");

				return provider.GetDataSize(variantNumber, type);
			}

			return type.GetSerializedProperties()
				.Select(propertyInfo =>
				{
					var provider = GetProvider(propertyInfo);
					var min = Mathf.Min(variantNumber, provider.GetDataVariants(propertyInfo.PropertyType) - 1);
					var dataSize = provider.GetDataSize(min, propertyInfo.PropertyType);
					return dataSize;
				})
				.Sum();
		}

		public static object GetData(int variantNumber, Type type)
		{
			var provider = DataProviders.FirstOrDefault(f => f.CanProvide(type));
			if (provider == null)
				throw new Exception($"[{nameof(DataProviderService)}] Cannot find data provider for type {type}");

			return provider.GetData(variantNumber, type);
		}

		public static void SetData(int variantNumber, object obj)
		{
			var type = obj.GetType();
			IsTypeConvertable(type);
			var propertyInfos = type.GetSerializedProperties();
			var dataProviders = propertyInfos
				.Select(GetProvider)
				.ToArray();

			for (var i = 0; i < dataProviders.Length; i++)
			{
				var propertyInfo = propertyInfos[i];
				var dataProvider = dataProviders[i];
				var variant = Mathf.Min(variantNumber, dataProvider.GetDataVariants(propertyInfo.PropertyType) - 1);
				dataProvider.SetData(variant, obj, propertyInfo);
			}
		}

		public static List<string> AssertDataEqual(int variantNumber, object obj)
		{
			var errors = new List<string>();
			var type = obj.GetType();
			if (type.GetInterface(typeof(IByteConvertable).FullName) == null)
			{
				var provider = DataProviders.FirstOrDefault(f => f.CanProvide(type));
				if (provider == null)
					throw new Exception($"[{nameof(DataProviderService)}] Cannot find data provider for type {type}");

				var variant = Mathf.Min(variantNumber, provider.GetDataVariants(type) - 1);
				return provider.AssertDataEqual(variant, obj, type.Name);
			}

			var propertyInfos = type.GetSerializedProperties();
			var dataProviders = propertyInfos
				.Select(GetProvider)
				.ToArray();

			for (var i = 0; i < dataProviders.Length; i++)
			{
				var propertyInfo = propertyInfos[i];
				var dataProvider = dataProviders[i];
				var variant = Mathf.Min(variantNumber, dataProvider.GetDataVariants(propertyInfo.PropertyType) - 1);
				var value = propertyInfo.GetValue(obj);
				errors.AddRange(dataProvider.AssertDataEqual(variant, value, propertyInfo.Name));
			}

			return errors;
		}

		private static IDataProvider GetProvider(PropertyInfo propertyInfo)
		{
			var provider = DataProviders.FirstOrDefault(f => f.CanProvide(propertyInfo.PropertyType));
			if (provider == null)
				throw new System.Exception(
					$"[{nameof(DataProviderService)}] Cannot find provider for data: {propertyInfo}");

			return provider;
		}

		private static void IsTypeConvertable(Type type)
		{
			if (type.GetInterface(typeof(IByteConvertable).FullName) == null)
				throw new System.Exception(
					$"[{nameof(DataProviderService)}] Type {type} do not implement interface {nameof(IByteConvertable)}");
		}
	}
}