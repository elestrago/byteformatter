﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Package.ByteFormatter.ByteValidator
{
	public interface IDataProvider
	{
		bool CanProvide(Type type);

		int GetDataVariants(Type type);

		int GetDataSize(int variantNumber, Type type);

		object GetData(int variantNumber, Type type);

		void SetData(int variantNumber, object obj, PropertyInfo propertyInfo);

		List<string> AssertDataEqual(int variantNumber, object obj, string name);
	}
}