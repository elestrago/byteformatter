﻿using System;

namespace ByteFormatter.Runtime.StateSerialize
{
	[AttributeUsage(AttributeTargets.Class)]
	public class ByteExtensionAttribute : Attribute
	{
	}
}