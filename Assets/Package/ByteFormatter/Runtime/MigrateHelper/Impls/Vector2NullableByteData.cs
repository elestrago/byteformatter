﻿using UnityEngine;

namespace ByteFormatter.Runtime.MigrateHelper.Impls
{
	[ByteDataTypeAccess(typeof(Vector2?))]
	public sealed class Vector2NullableByteData : AByteData<Vector2NullableByteData>
	{
		public override void Transfer(ByteReader reader, ByteWriter writer)
			=> writer.Write(reader.ReadVector2Nullable());

		public override void Skip(ByteReader reader) => reader.SkipVector2Nullable();
	}
}