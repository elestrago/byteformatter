using ByteFormatter.Runtime.StateSerialize;

namespace States
{
	[ByteDataContext(nameof(UserState))]
	public partial class UserState
	{
		[ByteDataIndex(0)] public UserAddressState Value { get; set; }
	}
}