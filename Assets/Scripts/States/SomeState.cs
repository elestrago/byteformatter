﻿using System.Collections.Generic;
using ByteFormatter.Runtime.StateSerialize;

namespace States
{
	[ByteDataContext(nameof(SomeState))]
	public partial class SomeState
	{
		[ByteDataIndex(0)] public int Value { get; set; }
		[ByteDataIndex(1)] public UserState User { get; set; }
		[ByteDataIndex(1)] public List<UserAddressState> Addresses { get; set; }

		public NotSerializedData Data { get; set; } 
	}

	public class NotSerializedData
	{
	}
}