using ByteFormatter.Runtime.StateSerialize;

namespace States
{
	[ByteDataContext(nameof(UserAddressState))]
	public partial class UserAddressState
	{
		[ByteDataIndex(0)] public int Value { get; set; }
	}
}